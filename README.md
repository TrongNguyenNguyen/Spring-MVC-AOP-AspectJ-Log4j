# Spring-MVC-AOP-AspectJ-Log4j
Spring MVC Log4j – Ghi log trong Spring MVC với Spring AOP, AspectJ, Log4j

Xem lại về Spring AOP: https://stackjava.com/spring/spring-aop-1.html

Xem lại về Spring AOP + AspectJ: https://stackjava.com/spring/spring-core-aop-aspectj.html

References:
https://docs.spring.io/spring/docs/current/spring-framework-reference
